

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Button,
  Alert,
  Icon,
} from 'react-native';
import MapView from 'react-native-maps';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
const App=()=>{
  return(
    <View>
      <ScrollView>
      <View style={styles.body}>
        <Image 
          style={styles.stretch}
          source={{
            uri: 'https://blockchainconsultora.com/images/white-logo.png',
          }}
        />        
      </View>
      <View style={styles.container}>     
       
        <View style={styles.containerText}>
          <View style={styles.containerElemento}>            
            <Image style={styles.imagen}
            source={{
              uri: 'https://reactnative.dev/img/tiny_logo.png',
            }}
           // style={styles.imagen} source={require('./assets/imagen2.png')}
            /> 
            <MapView           
              style={styles.elemento}
              region={{ latitude: -16.477854, 
                        longitude: -68.148326, 
                        latitudeDelta: 0.02, 
                        longitudeDelta: 0.079}}     >
              <MapView.Marker
                coordinate={{
                  latitude: -16.477854, 
                  longitude: -68.148326}}
                title={" "}
                description={"BLOCKCHAIN CONSULTORA"}
                />
            </MapView>
          </View>
          <Text style={styles.tituloPrincipal}>BLOCKCHAIN CONSULTORA</Text>
          <Text style={styles.titulo}>Sobre nosotros</Text>
          <Text style={styles.card}>
          Nuestro principal objetivo es la satisfacción del cliente, y para ello ofrecemos un servicio basado en la atención personalizada, 
          fomentando una relación con el cliente que no termina con la finalización del proyecto, sino que se refuerza con nuestro apoyo 
          posterior para resolver todo tipo de dudas y problemas que pudieran surgir, siempre buscando la mejor solución.
          </Text>          
          <Text  style={styles.card}>
          Luego desarrollamos aplicaciones que se ajusten a resolver exclusivamente esas necesidades,
          optimizando costos y tiempos.</Text>
          <Text style={styles.titulo}>servicio</Text>
          <Text style={styles.tituloPrincipal}>Apps móviles híbridas</Text>
          
          <Text style={styles.card}>
          Hacemos aplicaciones livianas y altamente optimizadas, que corren en ambos sistemas Operativos, IOS y Android. Desarrollo híbrido con altos estándares de rendimiento.
          </Text>
          <Button color="#31447B"
            title="contactanos"            
            onPress={() => {
              Alert.alert(
                "Correo",
                "info@blockchainconsultora.com",
                [
                  // {
                  //   text: "Cancelar",
                  //   onPress: () => console.log("Presinó Cancelar"),
                    
                  // },
                  { text: "OK", onPress: () => console.log("Presionó OK") }
                ],
                { cancelable: false }
              
              );
              console.log('Correo: info@blockchainconsultora.com');
            }}            
          />
        </View>          
      </View>
      </ScrollView>
      
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },  
  body: {
    backgroundColor: Colors.black,
  }, 
  container: {
    padding: 20,
    backgroundColor:'#0B3861',
  },
  containerText: {
    borderRadius:20,
    padding:20,
    backgroundColor:Colors.lighter,
  },  
  containerElemento:{
    //marginHorizontal:-20,
    //marginTop:-20,
    flexDirection: 'row',    
  },
  stretch: {
    margin:10,    
    width: 330,
    height: 40,
    resizeMode: 'stretch',
  },
  card: {
    marginBottom:15,
    textAlign:"justify",
    color: Colors.dark,    
  },
  
  tituloPrincipal:{
    fontSize:20,
    width:'100%',
    color:Colors.dark,
    margin:10,   

  },
  titulo:{
    fontSize:18,
    color:'#00BFFF',
    margin:10,
    paddingTop:20,
    borderTopWidth:1,
    borderColor:'#AFAFAF',
    
  },
  elemento:{    
     width: '50%', 
     height: 150, 
     marginLeft:5,   
     //resizeMode: 'stretch',   
  },
  imagen:{
    width: '50%',
    height: 150,
    borderRadius: 400/2,
    
    resizeMode:'stretch',
    transform: [{ rotate: '35deg' }]
  }
});

export default App;
